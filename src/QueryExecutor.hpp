/*
 * QueryExecutor.hpp
 *
 *  Created on: 01-Mar-2014
 *      Author: sriram
 */

#ifndef QUERYEXECUTOR_HPP_
#define QUERYEXECUTOR_HPP_

#include <map>
#include <string>
#include <vector>

typedef std::map<std::string, std::vector<std::string> > StringVectorMap;
typedef std::pair<std::string, std::vector<std::string> > StringVectorMapEntry;

class Column {
public:
	std::string columnName;
	std::string dataType;
	int capacity;
	int index;
	std::vector<std::string> operVector;
	std::vector<std::string> valueVector;
	std::vector<std::string> logicalOperVector;
	Column(std::string,std::string,int);
};


class QueryExecutor {
private:
	std::map<std::string, Column *> columnsMap;
public:
	const static std::string TABLES;
	const static std::string COLUMNS;
	const static std::string CONDITIONS;
	const static std::string JOIN;
	const static std::string ORDER_BY;

	void execute(StringVectorMap);
	virtual ~QueryExecutor();
};

#endif /* QUERYEXECUTOR_HPP_ */
