/*
 * QueryExecutor.cpp
 *
 *  Created on: 01-Mar-2014
 *      Author: sriram
 */

#include "QueryExecutor.hpp"

#include <algorithm/string/classification.hpp>
#include <algorithm/string/detail/classification.hpp>
#include <algorithm/string/predicate.hpp>
#include <algorithm/string/replace.hpp>
#include <algorithm/string/split.hpp>
#include <algorithm/string/trim.hpp>
#include <algorithm>
//#include <algorithm>
#include <cctype>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <iostream>
//#include <iterator>
#include <iterator>
#include <queue>
#include <set>
#include <sstream>
#include <utility>
//#include <utility>

#include "configurations.hpp"
#include "dbconstants.hpp"
#include "DBSystem.h"
#include "Utils.hpp"

using namespace std;

const string QueryExecutor::TABLES = "TABLES";
const string QueryExecutor::COLUMNS = "COLUMNS";
const string QueryExecutor::CONDITIONS = "CONDITIONS";
const string QueryExecutor::ORDER_BY = "ORDER BY";
const string QueryExecutor::JOIN = "JOIN";

Column::Column(string name, string type, int index) {
	capacity = 0;
	this->index = index;
	columnName = name;

	if (boost::iequals(type, TYPE_INTEGER) || boost::iequals(type, TYPE_FLOAT))
		transform(type.begin(), type.end(), dataType.begin(), ::toupper);
	else if (boost::iequals(type.substr(0, 7), TYPE_VARCHAR)) {
		dataType = TYPE_VARCHAR;

		istringstream iss(type.substr(8, type.length() - 8 - 1));
		iss >> capacity;
	}
}

QueryExecutor::~QueryExecutor() {
	map<string, Column *>::iterator it = columnsMap.begin();
	for (; it != columnsMap.end(); ++it) {
		Column *c = it->second;
		delete c;
	}

	columnsMap.clear();
}

int parseOperator(string condString, int index, string &oper) {
	/* Checking the operator */
	if (condString[0] == OP_LESS_THAN || condString[0] == OP_GREATER_THAN) {

		oper.push_back(condString[0]);
		index++;

		if (condString.length() > 1 && condString[1] == OP_EQUAL_TO) {
			oper.push_back(condString[1]);
			index++;
		}

	} else if (condString[0] == OP_EQUAL_TO) {
		oper.push_back(condString[0]);
		index++;

	} else {
		if (boost::iequals(condString, OP_LIKE)) {
			oper.append(OP_LIKE);
			index += 4;
		}
	}

	return index;
}

void parseConditions(string condString, map<string, Column *> &columnsMap) {
	int len = condString.length();
	int index;

	Column *currentColumn;
	string prevLogicalOper;

	for (index = 0; index < len;) {
		string operand;
		bool isRhs = false;

		char c;
		for (; index < len; index++) {
			c = condString[index];
			if (isalnum(c) || c == UNDERSCORE_CHAR || c == HYPHEN_CHAR)

				operand.push_back(condString[index]);

			else if (c == DOUBLE_QUOTE_CHAR || c == SINGLE_QUOTE_CHAR) {
				//Do nothing

			} else if (condString[index] == DOT_CHAR)
				operand = "";

			else {
				if (!isRhs) {
					currentColumn = columnsMap[operand];
					operand = "";

					if (condString[index] == SPACE_CHAR)
						index++;

					string oper;
					index = parseOperator(condString.substr(index, 4), index,
							oper);
					currentColumn->operVector.push_back(oper);

					isRhs = true;

					if (condString[index] != SPACE_CHAR)
						index--;

				} else
					break;
			}
		}

		if (isRhs)
			currentColumn->valueVector.push_back(operand);

		if (index < len) {
			if (condString[index] == SPACE_CHAR)
				index++;

			if (boost::iequals(condString.substr(index, 2), "OR")) {
				index += 2;
				currentColumn->logicalOperVector.push_back("OR");
			} else if (boost::iequals(condString.substr(index, 3), "AND")) {
				index += 3;
				currentColumn->logicalOperVector.push_back("AND");
			}

			if (condString[index] == SPACE_CHAR)
				index++;

		} else {
			if (!prevLogicalOper.empty())
				currentColumn->logicalOperVector.push_back(prevLogicalOper);
			else
				currentColumn->logicalOperVector.push_back("OR");
		}

		prevLogicalOper = currentColumn->logicalOperVector.back();
	}
}

map<string, Column *> parseColumns(string schema) {
	map<string, Column *> columnsMap;
	vector<string> splitStrings;
	boost::split(splitStrings, schema, boost::is_any_of(":,"));

	int size = splitStrings.size();
	int i;
	for (i = 0; i < size; i += 2) {
		Column *c = new Column(splitStrings[i], splitStrings[i + 1], i / 2);
		columnsMap[splitStrings[i]] = c;
	}

	return columnsMap;
}

bool evaluate(string operand1, string operand2, string oper, string type) {
	if (oper.length() > 1) {
		if (oper == "<=")
			return atof(operand1.c_str()) <= atof(operand2.c_str());
		else if (oper == ">=")
			return atof(operand1.c_str()) >= atof(operand2.c_str());
		else if (oper == "LIKE")
			return boost::iequals(operand1, operand2);

	} else {
		if (oper == "<")
			return atof(operand1.c_str()) < atof(operand2.c_str());
		else if (oper == ">")
			return atof(operand1.c_str()) > atof(operand2.c_str());
		else if (oper == "=") {
			if (type == TYPE_INTEGER || type == TYPE_FLOAT)
				return atof(operand1.c_str()) == atof(operand2.c_str());
			else
				return boost::equals(operand1, operand2);
		}
	}

	return false;
}

class OrderByComparator {
private:
	map<std::string, Column *> comparatorColumnsMap;
	StringVectorMap comparatorParamsMap;
	bool isReversed;

public:
	bool operator()(const vector<string> &a, const vector<string> &b) {

		vector<string> listOfOrderBy =
				comparatorParamsMap[QueryExecutor::ORDER_BY];
		for (vector<string>::iterator columnName = listOfOrderBy.begin();
				columnName != listOfOrderBy.end(); columnName++) {

			int columnIndex = comparatorColumnsMap[*columnName]->index;
			string typeOfColumn = Utils::type(*columnName,
					comparatorParamsMap[QueryExecutor::TABLES]);

			if (boost::iequals(typeOfColumn.substr(0, 7), TYPE_VARCHAR)) {
				string as = a[columnIndex];
				string bs = b[columnIndex];

				int strCompareValue = as.compare(bs);
				if (strCompareValue == 0) {

				} else {
					if (isReversed) {
						return strCompareValue < 0 ? false : true;
					}
					return strCompareValue < 0 ? true : false;
				}

			} else {
				float af = atof(a[columnIndex].c_str());
				float bf = atof(b[columnIndex].c_str());

				if (af == bf) {

				} else {
					if (isReversed) {
						return af > bf;
					}
					return af < bf;
				}

			}
		}

		if (isReversed) {
			return false;
		}
		return true;
	}
	OrderByComparator(map<string, Column *> map1,
			StringVectorMap comparatorParamsMapTemp) {
		this->comparatorColumnsMap = map1;
		this->comparatorParamsMap = comparatorParamsMapTemp;
		isReversed = false;
	}

	OrderByComparator(map<string, Column *> map1,
			StringVectorMap comparatorParamsMapTemp, bool isRev) {
		this->comparatorColumnsMap = map1;
		this->comparatorParamsMap = comparatorParamsMapTemp;
		this->isReversed = isRev;
	}
};

void printTupleVector(vector<string> tupleVector, StringVectorMap paramsMap,
		map<string, Column *> columnsMap) {

	int columnIndex;
	string columnName;

	for (unsigned int columnsIt = 0;
			columnsIt < paramsMap[QueryExecutor::COLUMNS].size(); columnsIt++) {

		if (columnsIt > 0)
			cout << ",";

		columnName = paramsMap[QueryExecutor::COLUMNS][columnsIt];
		columnIndex = columnsMap[columnName]->index;
		//if (isalnum(tupleVector[columnIndex][0]))
		cout << DOUBLE_QUOTE_CHAR << tupleVector[columnIndex]
				<< DOUBLE_QUOTE_CHAR;

	}

	cout << endl;

	/*
	 vector<string>::iterator column = (tupleVector).begin();

	 if (!(*column).empty()) {
	 cout << DOUBLE_QUOTE_CHAR << *column << DOUBLE_QUOTE_CHAR;
	 for (column++; column != (tupleVector).end() - 1; column++)
	 cout << "," << DOUBLE_QUOTE_CHAR << *column << DOUBLE_QUOTE_CHAR;

	 cout << endl;
	 } */
}

void sortAndDumpTuples(vector<vector<string> > &listOfTuples, int fileNumber,
		StringVectorMap paramsMap, map<string, Column *> columnsMap) {

	OrderByComparator comparator(columnsMap, paramsMap);
	std::sort(listOfTuples.begin(), listOfTuples.end(), comparator);

	string tempFileName = "/tmp/";
	tempFileName.append(paramsMap[QueryExecutor::TABLES][0]);
	stringstream ss;
	ss << fileNumber;
	tempFileName.append(ss.str());

	ofstream dataFile(tempFileName.c_str(), ios::out);

	for (vector<vector<string> >::iterator tuple = listOfTuples.begin();
			tuple != listOfTuples.end(); tuple++) {

		vector<string>::iterator column = (*tuple).begin();
		dataFile << *column;
		for (column++; column != (*tuple).end(); column++) {
			dataFile << "," << *column;
		}
		dataFile << endl;

	}
	dataFile.close();
}

void mergeFilesAndPrint(int fileCount, StringVectorMap paramsMap,
		map<string, Column *> columnsMap) {
	ifstream files[fileCount];

	std::priority_queue<vector<string>, std::vector<vector<string> >,
			OrderByComparator> queue(
			OrderByComparator(columnsMap, paramsMap, true));

	/* Merge */
	for (int i = 0; i < fileCount; i++) {
		string tempFileName = "/tmp/";
		tempFileName.append(paramsMap[QueryExecutor::TABLES][0]);
		tempFileName.append(Utils::toString(i));
		files[i].open(tempFileName.c_str());

		string lineRead;
		getline(files[i], lineRead);
		boost::trim(lineRead);

		if (!lineRead.empty()) {
			lineRead.append(",");
			lineRead.append(Utils::toString(i));

			vector<string> splitStringVector;
			boost::split(splitStringVector, lineRead, boost::is_any_of(","));

			queue.push(splitStringVector);
		}
	}

	int availableFile = fileCount;
	while (availableFile > 0) {
		vector<string> lineVector = queue.top();
		queue.pop();

		printTupleVector(lineVector, paramsMap, columnsMap);

		int lastindex = atoi(lineVector[lineVector.size() - 1].c_str());

		string nextLine;
		getline(files[lastindex], nextLine);
		boost::trim(nextLine);

		if (!nextLine.empty()) {
			nextLine.append(",");
			nextLine.append(lineVector[lineVector.size() - 1]);

			vector<string> splitString;
			boost::split(splitString, nextLine, boost::is_any_of(","));

			if (!splitString.empty())
				queue.push(splitString);
		}

		if (files[lastindex].eof())
			availableFile--;
	}

	while (!queue.empty()) {
		vector<string> lineVector = queue.top();
		queue.pop();

		if (!lineVector.empty())
			printTupleVector(lineVector, paramsMap, columnsMap);
	}

	for (int i = 0; i < fileCount; i++) {
		files[i].close();
	}

	// TODO: Delete temp files
	/**
	 * Have to figure out the way to delete the file.
	 */

}

class TableChunk {
private:

public:
	std::ifstream *chunkReaderStream;
	int fileNumber;
	string currentLine;

	TableChunk(int number) :
			chunkReaderStream(NULL), fileNumber(number) {
	}

	TableChunk(const TableChunk &chunk) :
			chunkReaderStream(chunk.chunkReaderStream) {
		fileNumber = chunk.fileNumber;
		currentLine = chunk.currentLine;
	}

	/* TableChunk& operator=(const TableChunk &rhs) {
	 chunkReaderStream = rhs.chunkReaderStream;
	 fileNumber = rhs.fileNumber;
	 currentLine = rhs.currentLine;
	 return *this;
	 } */

	void openChunkReaderStream(string dirPath, string tableName) {
		string filePath = dirPath + tableName + Utils::toString(fileNumber);
		chunkReaderStream = new ifstream(filePath.c_str());
	}

	void closeChunkReaderStream() {
		chunkReaderStream->close();
	}

	string nextLine() {
		if (chunkReaderStream == NULL) {
			cerr << "You need to open the file first." << endl;
			return "";
		}

		string line;
		while (line.empty()) {
			if (chunkReaderStream->eof())
				return "NULL";

			getline(*chunkReaderStream, line);
			boost::trim(line);
		}

		currentLine = line;
		return line;
	}

	string getCurrentLine() {
		return currentLine;
	}

	vector<string> columnValuesVector() const {

		vector<string> splitsVector;

		if (currentLine != "NULL" && !currentLine.empty()) {
			boost::split(splitsVector, currentLine, boost::is_any_of(","));
			//splitsVector.pop_back();
		}
		return splitsVector;
	}
};

class JoinComparator {
private:
	int columnIndex;
	string typeOfColumn;
public:

	JoinComparator() {

	}

	JoinComparator(int _columnIndex, string _type) {
		this->columnIndex = _columnIndex;
		this->typeOfColumn = _type;

	}

	bool operator()(const TableChunk &split1, const TableChunk &split2) {
		vector<string> a = split1.columnValuesVector();
		vector<string> b = split2.columnValuesVector();

		if (boost::iequals(typeOfColumn.substr(0, 7), TYPE_VARCHAR)) {
			string as = a[this->columnIndex];
			string bs = b[this->columnIndex];

			int strCompareValue = as.compare(bs);
			if (strCompareValue == 0) {

			} else {
				return strCompareValue < 0 ? true : false;
			}

		} else {
			float af = atof(a[this->columnIndex].c_str());
			float bf = atof(b[this->columnIndex].c_str());

			if (af == bf) {

			} else {
				return af < bf;
			}

		}

		return true;

	}
};

class SortComparator {
private:
	int columnIndex;
	string typeOfColumn;
public:
	SortComparator(int _columnIndex, string _type) {
		this->columnIndex = _columnIndex;
		this->typeOfColumn = _type;

	}

	bool operator()(const vector<string> &a, const vector<string> &b) {

		if (boost::iequals(typeOfColumn.substr(0, 7), TYPE_VARCHAR)) {
//			cout << "In if" << endl;

			string as = a[this->columnIndex];
			string bs = b[this->columnIndex];

			int strCompareValue = as.compare(bs);
			return strCompareValue < 0 ? true : false;

		} else {
			/* cout << "In else" << endl;

			 for (unsigned i = 0; i < a.size(); i++) {
			 cout << a[i] << ",";
			 }
			 cout << endl;

			 for (unsigned i = 0; i < b.size(); i++) {
			 cout << b[i] << ",";
			 }
			 cout << endl;

			 cout << a.size() << " " << b.size() << " " << this->columnIndex
			 << endl; */

			float af = atof(a[this->columnIndex].c_str());
			float bf = atof(b[this->columnIndex].c_str());

			return af < bf;
		}

//		cout << "Out comp" << endl;
		return true;
	}
};

void dumpVectorOfVector(vector<vector<string> > listOfTuples, int file_number,
		string tableName) {

	string filename = "/tmp/" + tableName + Utils::toString(file_number);
	ofstream outputFile;
	outputFile.open(filename.c_str());

	//cout << "==============================" << endl;
	for (vector<vector<string> >::iterator eachTuple = listOfTuples.begin();
			eachTuple != listOfTuples.end(); eachTuple++) {
		vector<string> eachTupleVal = *eachTuple;
		bool addComma = false;

		for (vector<string>::iterator eachColumn = eachTupleVal.begin();
				eachColumn != eachTupleVal.end(); eachColumn++) {

			if(addComma)
				outputFile << ",";

			outputFile << *eachColumn;
			addComma = true;
		}
		outputFile << endl;
	}
	//cout << "==============================" << endl;

	outputFile.close();
}

int createSortedSublist(string tableName, string columnName) {

	int file_count = 0;
	int lineNumber = -1;
	int count_of_bytes = 0;

	int columnIndex = Utils::columnIndexInTable(columnName, tableName);
	string columnType = Utils::columnTypeInTable(columnName, tableName);

	DBSystem *dbSystem = DBSystem::getInstance();
	vector<vector<string> > listOfTuples;

	while (true) {

		lineNumber++;
		string tuple = dbSystem->getRecord(tableName, lineNumber);
		boost::trim(tuple);

		if (tuple == "NULL")
			break;

		if (tuple.empty() || tuple == "")
			continue;

		int tuple_length = strlen(tuple.c_str());

		if ((count_of_bytes + tuple_length)
				<= (Configurations::numberOfPages * Configurations::pageSize)) {

			count_of_bytes += tuple_length;

			vector<string> splitStringVector;
			boost::split(splitStringVector, tuple, boost::is_any_of(","));

			if (!splitStringVector.empty())
				listOfTuples.push_back(splitStringVector);

		} else {
			// sort and dump it to a sublist

			SortComparator comparator(columnIndex, columnType);
			std::sort(listOfTuples.begin(), listOfTuples.end(), comparator);

			dumpVectorOfVector(listOfTuples, file_count, tableName);
			file_count++;

			listOfTuples.clear();

			vector<string> splitStringVector;
			boost::split(splitStringVector, tuple, boost::is_any_of(","));

			if (!splitStringVector.empty())
				listOfTuples.push_back(splitStringVector);

			count_of_bytes = tuple_length;
		}

	}

	if (listOfTuples.size() != 0) {
		SortComparator comparator(columnIndex, columnType);
		std::sort(listOfTuples.begin(), listOfTuples.end(), comparator);

		dumpVectorOfVector(listOfTuples, file_count, tableName);
		file_count++;

		listOfTuples.clear();
		count_of_bytes = 0;
	}

	return file_count;
}

int compare(string val1, string type1, string val2, string type2) {

	if (type1 == TYPE_VARCHAR) {
		return val1.compare(val2);

	} else {
		double dVal1 = atof(val1.c_str());
		double dVal2 = atof(val2.c_str());

		if (dVal1 < dVal2)
			return -1;
		else if (dVal1 > dVal2)
			return 1;

		return 0;
	}

}

class PriorityQueueManager {
private:
	string tableName;
	int columnIndex;
	string columnType;
	int totalChunksForTable;
	priority_queue<TableChunk, vector<TableChunk>, JoinComparator> priorityQueue;

public:

	PriorityQueueManager(string name, int totalNumberOfChunks, int index,
			string type) :
			tableName(name), columnIndex(index), columnType(type), totalChunksForTable(
					totalNumberOfChunks) {
		//columnIndex--;
		init();
	}

	void init() {
		for (int chunkId = 0; chunkId < totalChunksForTable; chunkId++) {

			TableChunk chunk(chunkId);
			chunk.openChunkReaderStream("/tmp/", tableName);

			string lineRead = chunk.nextLine();

			if (lineRead == "NULL")
				continue;

//			vector<string> splitStringVector;
//			boost::split(splitStringVector, lineRead, boost::is_any_of(","));

			priorityQueue.push(chunk);
		}
	}

	set<vector<string> > nextSet() {
		set<vector<string> > sameValuesSet;

		vector<string> valuesVector = nextRecord();
		if (valuesVector.empty())
			return sameValuesSet;

		sameValuesSet.insert(valuesVector);

		while (!priorityQueue.empty()) {
			TableChunk chunk = priorityQueue.top();
			vector<string> nextValuesVector = chunk.columnValuesVector();
			if (nextValuesVector.empty())
				break;

			if (compare(valuesVector[columnIndex], columnType,
					nextValuesVector[columnIndex], columnType) == 0) {

				nextRecord(); // Removing the top element and adding new one with next line
				sameValuesSet.insert(nextValuesVector);
			} else
				break;
		}

		return sameValuesSet;
	}

	vector<string> nextRecord() {
		vector<string> recordVector;
		if (priorityQueue.empty())
			return recordVector;

		TableChunk chunk = priorityQueue.top();
		priorityQueue.pop();

		recordVector = chunk.columnValuesVector();

		/* Fetching next line from the chunk and inserting it back into the priority queue */
		string line = chunk.nextLine();
		if (!line.empty() && line != "NULL")
			priorityQueue.push(chunk);

		return recordVector;
	}

	bool isEmpty() {
		return priorityQueue.empty();
	}
};

void mergeAndPrintJoin(vector<string> &tableNamesVector,
		vector<int> &tableChunkCountsVector,
		vector<int> &joinColumnIndicesVector,
		vector<string> &joinColumnTypeVector,
		vector<string> &projectColumnsVector) {

	/* creating priority_queue for each table */
	PriorityQueueManager *firstPQManager, *secondPQManager;
	firstPQManager = new PriorityQueueManager(tableNamesVector[0],
			tableChunkCountsVector[0], joinColumnIndicesVector[0],
			joinColumnTypeVector[0]);

	secondPQManager = new PriorityQueueManager(tableNamesVector[1],
			tableChunkCountsVector[1], joinColumnIndicesVector[1],
			joinColumnTypeVector[1]);

	map<string, string> projectColumnsIndicesMap;
	for (unsigned int i = 0; i < projectColumnsVector.size(); i++) {
		string tableName, columnName;

		unsigned int indexOfDot = projectColumnsVector[i].find('.');
		if (indexOfDot == std::string::npos) {
			// If dot does not exist then 0 is of the left table and 1 is for the right table.
			int columnIndex = Utils::columnIndexInTable(projectColumnsVector[i],
					tableNamesVector[0]);
			if (columnIndex == -1)
				tableName = tableNamesVector[1];
			else
				tableName = tableNamesVector[0];

			columnName = projectColumnsVector[i];

		} else {
			// If dot exist then left is the column name and right is the column name.
			tableName = projectColumnsVector[i].substr(0, indexOfDot);
			columnName = projectColumnsVector[i].substr(indexOfDot + 1);
		}

		unsigned tableNameIndex;
		for (tableNameIndex = 0; tableNameIndex < tableNamesVector.size();
				tableNameIndex++)
			if (tableNamesVector[tableNameIndex] == tableName)
				break;

		string key = projectColumnsVector[i];
		projectColumnsIndicesMap[key] = Utils::toString(tableNameIndex) + "_"
				+ Utils::toString(
						Utils::columnIndexInTable(columnName, tableName));

		if (i > 0)
			cout << COMMA_CHAR;
		cout << DOUBLE_QUOTE_CHAR << projectColumnsVector[i]
				<< DOUBLE_QUOTE_CHAR;
	}

	cout << endl;

	vector<string> recordFromSecondTable;
	while (!firstPQManager->isEmpty()) {
		set<vector<string> > sameValueRecordsSet = firstPQManager->nextSet();

		for (set<vector<string> >::iterator setItr =
				sameValueRecordsSet.begin();
				setItr != sameValueRecordsSet.end(); ++setItr) {
			vector<string> recordVector = *setItr;

			// Record from previous iteration
			if (!recordFromSecondTable.empty()) {
				int compareResult = compare(
						recordVector[joinColumnIndicesVector[0]],
						joinColumnTypeVector[0],
						recordFromSecondTable[joinColumnIndicesVector[1]],
						joinColumnTypeVector[1]);

				if (compareResult == 0) {
					for (set<vector<string> >::iterator innerSetItr =
							sameValueRecordsSet.begin();
							innerSetItr != sameValueRecordsSet.end();
							++innerSetItr) {

						for (unsigned int i = 0;
								i < projectColumnsVector.size(); i++) {
							string data =
									projectColumnsIndicesMap[projectColumnsVector[i]];
							vector<string> splitIndexVector;
							boost::split(splitIndexVector, data,
									boost::is_any_of("_"));

							if (i > 0)
								cout << COMMA_CHAR;

							switch (atoi(splitIndexVector[0].c_str())) {
							case 0:
								cout
										<< (*innerSetItr)[atoi(
												splitIndexVector[1].c_str())];
								break;

							case 1:
								cout
										<< recordFromSecondTable[atoi(
												splitIndexVector[1].c_str())];
								break;
							}
						}

						cout << endl;
					}
				} else if (compareResult < 0)
					continue;

				recordFromSecondTable.clear();
			}

			while (!secondPQManager->isEmpty()) {
				recordFromSecondTable = secondPQManager->nextRecord();

				int compareResult = compare(
						recordVector[joinColumnIndicesVector[0]],
						joinColumnTypeVector[0],
						recordFromSecondTable[joinColumnIndicesVector[1]],
						joinColumnTypeVector[1]);

				if (compareResult == 0) {
					for (set<vector<string> >::iterator innerSetItr =
							sameValueRecordsSet.begin();
							innerSetItr != sameValueRecordsSet.end();
							++innerSetItr) {
						for (unsigned int i = 0;
								i < projectColumnsVector.size(); i++) {
							string data =
									projectColumnsIndicesMap[projectColumnsVector[i]];
							vector<string> splitIndexVector;
							boost::split(splitIndexVector, data,
									boost::is_any_of("_"));

							if (i > 0)
								cout << COMMA_CHAR;

							switch (atoi(splitIndexVector[0].c_str())) {
							case 0:
								cout
										<< (*innerSetItr)[atoi(
												splitIndexVector[1].c_str())];
								break;

							case 1:
								cout
										<< recordFromSecondTable[atoi(
												splitIndexVector[1].c_str())];
								break;
							}
						}

						cout << endl;
					}

				} else if (compareResult < 0) {

					break;
				}
			}
		}
	}
}

void sortMergeJoin(map<string, string> &tableColumnMap,
		vector<string> &tableNamesVector, vector<int> &joinColumnIndicesVector,
		vector<string> &joinColumnTypeVector, vector<string> &listOfColumns) {

	vector<int> tableChunksCountVector;
//	vector<string> tablesVector;
	for (unsigned i = 0; i < tableNamesVector.size(); i++) {

//		tablesVector.push_back(itr->first);
		string columnName = tableColumnMap[tableNamesVector[i]];

		int file_count = createSortedSublist(tableNamesVector[i], columnName);
		tableChunksCountVector.push_back(file_count);
	}

	cout << "Before merge" << endl;
	mergeAndPrintJoin(tableNamesVector, tableChunksCountVector,
			joinColumnIndicesVector, joinColumnTypeVector, listOfColumns);
	cout << "After merge" << endl;
}

void printParenthesis(vector<string> &tableNamesVector, int ** s, int i,
		int j) {
	if (i == j) {
		if (i > 0 && tableNamesVector[i] == tableNamesVector[i - 1])
			cout << tableNamesVector[i + 1] << ",";
		else
			cout << tableNamesVector[i] << ",";
	} else {
		cout << "(";
		printParenthesis(tableNamesVector, s, i, s[i][j]);
		printParenthesis(tableNamesVector, s, s[i][j] + 1, j);
		cout << ")";
	}

}

// Matrix Ai has dimension p[i-1] x p[i] for i = 1..n
int MatrixChainOrder(vector<string> tableNamesVector,
		vector<int> distinctValuesCountVector) {

	for (unsigned i = 0; i < tableNamesVector.size(); i += 2) {
		string table1 = tableNamesVector[i];
		string table2 = tableNamesVector[i + 1];
		string table3 = tableNamesVector[i + 2];
		string table4 = tableNamesVector[i + 3];

		if (table1 == table3 || table1 == table4) {
			tableNamesVector[i] = table2;
			tableNamesVector[i + 1] = table1;

			int temp = distinctValuesCountVector[i];
			distinctValuesCountVector[i] = distinctValuesCountVector[i + 1];
			distinctValuesCountVector[i + 1] = temp;

			if (table1 == table4) {
				tableNamesVector[i + 2] = table4;
				tableNamesVector[i + 3] = table3;

				temp = distinctValuesCountVector[i + 2];
				distinctValuesCountVector[i + 2] = distinctValuesCountVector[i
						+ 3];
				distinctValuesCountVector[i + 3] = temp;
			}
		}
	}

	std::reverse(tableNamesVector.begin(), tableNamesVector.end());
	std::reverse(distinctValuesCountVector.begin(),
			distinctValuesCountVector.end());

	int n = (tableNamesVector.size() / 2) + 1;

	/* For simplicity of the program, one extra row and one extra column are
	 allocated in m[][].  0th row and 0th column of m[][] are not used */
	double m[n][n];
	int ** s;
	s = (int **) calloc(n, sizeof(int *));
	for (int i = 0; i < n; i++) {
		s[i] = (int *) calloc(n, sizeof(int));
	}

	int i, j, k, L, q;

	/* m[i,j] = Minimum number of scalar multiplications needed to compute
	 the matrix A[i]A[i+1]...A[j] = A[i..j] where dimention of A[i] is
	 p[i-1] x p[i] */

	// cost is zero when multiplying one matrix.
	for (i = 0; i < n; i++) {
		for (j = 0; j < n; j++) {
			m[i][j] = 0;
		}
	}

	// L is chain length.
	for (L = 2; L <= n; L++) {
		for (i = 0; i < n - L + 1; i++) {
			j = i + L - 1;
			m[i][j] = (double) INT_MAX;
			for (k = i; k < j; k++) {
				// q = cost/scalar multiplications

				int denom =
						(distinctValuesCountVector[k * 2]
								> distinctValuesCountVector[k * 2 + 1]) ?
								distinctValuesCountVector[k * 2] :
								distinctValuesCountVector[k * 2 + 1];

				long numer = 0;
				if (i == k) {
					if (k + 1 == j) {
						numer =
								Configurations::getTableDataForTable(
										tableNamesVector[k * 2])->getTotalRecords()
										* Configurations::getTableDataForTable(
												tableNamesVector[k * 2 + 1])->getTotalRecords();
					} else {
						numer = Configurations::getTableDataForTable(
								tableNamesVector[k * 2])->getTotalRecords()
								* m[k + 1][j];
					}
				} else {
					if (k + 1 == j) {
						numer =
								m[i][k]
										* Configurations::getTableDataForTable(
												tableNamesVector[k * 2 + 1])->getTotalRecords();
					} else {
						numer = m[i][k] * m[k + 1][j];
					}
				}

				q = m[i][k] + m[k + 1][j] + (numer * 1.0 / denom);

				if (q < m[i][j]) {
					m[i][j] = q;
					s[i][j] = k;
				}
			}

		}
	}

	/*
	 for (int i = 0; i < n; i++) {
	 for (int j = 0; j < n; j++) {
	 cout << m[i][j] << " ";
	 }
	 cout << endl;
	 }
	 */

	printParenthesis(tableNamesVector, s, 0, n - 1);

	return m[1][n - 1];
}

void estimateJoinOrderAndCost(vector<string> &tableNamesVector,
		vector<string> &joinColumnNamesVector,
		vector<int> &joinColumnIndicesVector,
		vector<string> &joinColumnTypeVector, vector<string> &listOfColumns) {

	DBSystem *db = DBSystem::getInstance();

	vector<int> distinctVector;

	for (unsigned int i = 0; i < tableNamesVector.size(); i++) {
		//cout << "Total rows in table " << tableNamesVector[i] << " : "
		//		<< Configurations::getTableDataForTable(tableNamesVector[i])->getTotalRecords()
		//		<< endl;

		int distinctValuesCount = db->V(tableNamesVector[i],
				joinColumnNamesVector[i]);
		//cout << "Distinct values in table " << tableNamesVector[i]
		//		<< " for column " << joinColumnNamesVector[i] << " : "
		//		<< distinctValuesCount << endl;

		distinctVector.push_back(distinctValuesCount);
	}

	MatrixChainOrder(tableNamesVector, distinctVector);
}

void performJoin(StringVectorMap &paramsMap) {
	vector<string> listOfTables = paramsMap[QueryExecutor::TABLES];
	vector<string> joinConditionsVector = paramsMap[QueryExecutor::JOIN];

	map<string, string> tableColumnMap;
	vector<string> tableNamesVector;
	vector<string> joinColumnTypeVector;
	vector<int> joinColumnIndicesVector;
	vector<string> joinColumnNamesVector;

	for (unsigned int j = 0; j < joinConditionsVector.size(); j++) {
		vector<string> listJoinColumns;
		boost::split(listJoinColumns, joinConditionsVector[j],
				boost::is_any_of("="));

		for (unsigned int i = 0; i < listJoinColumns.size(); i++) {

			string tableName, columnName;
			string listJoinColumnVal = listJoinColumns[i];
			boost::trim(listJoinColumnVal);

			unsigned int indexOfDot = ((string) listJoinColumnVal).find('.');
			if (indexOfDot == std::string::npos) {
				// If dot does not exist then 0 is of the left table and 1 is for the right table.
				tableName = listOfTables[i];
				columnName = listJoinColumnVal;

			} else {
				// If dot exist then left is the column name and right is the column name.
				tableName = listJoinColumnVal.substr(0, indexOfDot);
				columnName = listJoinColumnVal.substr(indexOfDot + 1);
			}

			// Check if column is present in the table or if the table is present
			if (!Utils::isTablePresent(tableName)) {
				// error
				cout << "Table : " << tableName << " not present" << endl;
				return;
			}

			int columnIndex =
					Configurations::getTableDataForTable(tableName)->columnIndexMap[columnName];//Utils::columnIndexInTable(columnName, tableName);
			if (columnIndex == -1) {
				// error
				cout << "Column : " << columnName
						<< " not present in the table : " << tableName << endl;
				return;
			}

			tableNamesVector.push_back(tableName);
			joinColumnIndicesVector.push_back(columnIndex);
			joinColumnTypeVector.push_back(
					Utils::columnTypeInTable(columnName, tableName));
			joinColumnNamesVector.push_back(columnName);

			tableColumnMap[tableName] = columnName;

			if (i > 0) {
				if (joinColumnTypeVector[i] != joinColumnTypeVector[i - 1]) {
					cerr << "The column data types do not match. Invalid query."
							<< endl;
					return;
				}
			}
		}
	}

	if (paramsMap[QueryExecutor::TABLES].size() > 2) {
		//cout << "Evaluate Part C and find out the optimum join sequence"
		//<< endl;

		estimateJoinOrderAndCost(tableNamesVector, joinColumnNamesVector,
				joinColumnIndicesVector, joinColumnTypeVector,
				paramsMap[QueryExecutor::COLUMNS]);
	} else
		sortMergeJoin(tableColumnMap, tableNamesVector, joinColumnIndicesVector,
				joinColumnTypeVector, paramsMap[QueryExecutor::COLUMNS]);
}

// TODO: column names of format table.column are not considered
void QueryExecutor::execute(StringVectorMap paramsMap) {

	if (!paramsMap[JOIN].empty()) {
		// Join condition exists
		/* vector<string> listOfTables = paramsMap[TABLES];
		 cout << "Joining tables : " << endl;
		 for (vector<string>::iterator itr = listOfTables.begin();
		 itr != listOfTables.end(); itr++) {
		 cout << *itr << endl;
		 }
		 cout << "Join condition : " << joinConditionStr << endl; */

		performJoin(paramsMap);
		return;
	}

	string fileName = Configurations::pathForData;
	fileName.append("/");
	fileName.append(paramsMap[TABLES][0]);
	fileName.append(".data");

	ifstream dataReader(fileName.c_str(), ifstream::in);

// TODO: Check if the table exists

	string schema;
	getline(dataReader, schema);
	dataReader.close();

	columnsMap = parseColumns(schema);

	bool hasConditions = false;
	if (paramsMap[CONDITIONS][0].length() > 0) {
		parseConditions(paramsMap[CONDITIONS][0], columnsMap);
		hasConditions = true;
	}

	string tuple;
	vector<vector<string> > listOfTuples;

	long long bytesRead = 0;
	int fileCount = 0;
	const long long totalMemory = Configurations::pageSize
			* Configurations::numberOfPages;

	bool hasOrderBy = (paramsMap[ORDER_BY][0].length() > 0);

	DBSystem *dbSystem = DBSystem::getInstance();

	int lineNumber = 0;

	for (unsigned int i = 0; i < paramsMap[COLUMNS].size(); i++) {
		if (i > 0)
			cout << COMMA_CHAR;
		cout << DOUBLE_QUOTE_CHAR << paramsMap[COLUMNS][i] << DOUBLE_QUOTE_CHAR;
	}

	cout << endl;

	while (true) {
		tuple = dbSystem->getRecord(paramsMap[TABLES][0], lineNumber);

		if (tuple == "NULL")
			break;

		boost::replace_all(tuple, "\"", "");

		lineNumber++;

		vector<string> attrVector;
		boost::split(attrVector, tuple, boost::is_any_of(","));

		string columnName;
		int columnIndex;

		bool finalTruthValue;
		if (hasConditions) {
			map<string, Column *>::iterator mapIt = columnsMap.begin();
			bool firstCondition = true;

			for (; mapIt != columnsMap.end(); ++mapIt) {
				if (mapIt->second->operVector.empty())
					continue;

				Column *currCol = mapIt->second;
				columnIndex = columnsMap[currCol->columnName]->index;

				int operIt;
				// process condition
				for (operIt = 0; operIt < currCol->operVector.size();
						operIt++) {
					string oper = currCol->operVector[operIt];
					bool truthValue = evaluate(attrVector[columnIndex],
							currCol->valueVector[operIt], oper,
							currCol->dataType);

					if (firstCondition) {
						firstCondition = false;
						finalTruthValue = truthValue;
						/* All AND so if this is false then eliminate evaluating other conditions */
						if (currCol->logicalOperVector[operIt] == "AND"
								&& !finalTruthValue)
							break;

					} else {
						if (currCol->logicalOperVector[operIt] == "AND")
							finalTruthValue = finalTruthValue && truthValue;
						else if (currCol->logicalOperVector[operIt] == "OR")
							finalTruthValue = finalTruthValue || truthValue;
					}
				}

				if (operIt < currCol->operVector.size())
					break;
			}
		} else
			finalTruthValue = true;

		if (finalTruthValue) {

			if (hasOrderBy) {
				if (bytesRead + tuple.length() > totalMemory) {

					// sort and dump
					bytesRead = 0;

					sortAndDumpTuples(listOfTuples, fileCount, paramsMap,
							columnsMap);

					fileCount++;
					listOfTuples.clear();
				}

				vector<string> splitStringVector;
				boost::split(splitStringVector, tuple, boost::is_any_of(","));
				listOfTuples.push_back(splitStringVector);
				bytesRead += tuple.length();
			} else {
				printTupleVector(attrVector, paramsMap, columnsMap);
			}
		}

	}

	if (hasOrderBy) {
		if (listOfTuples.size() > 0) {
			sortAndDumpTuples(listOfTuples, fileCount, paramsMap, columnsMap);
			fileCount++;
			listOfTuples.clear();
		}

		mergeFilesAndPrint(fileCount, paramsMap, columnsMap);

	}
}
