/*
 * DBSystem.h
 *
 *  Created on: 25-Jan-2014
 *      Author: arpit
 */

#ifndef DBSYSTEM_H_
#define DBSYSTEM_H_

#include <string>
#include <vector>

struct LineOffset {
public:
	int lineNumber;
	long fileOffset;
	
	LineOffset() : lineNumber(0), fileOffset(0) {}
	
	LineOffset(int num, long offset) : lineNumber(num), fileOffset(offset) {}
};

class DBSystem
{
public:
	
	virtual ~DBSystem();
	
	void populateDBInfo();
	void populateDBInfo(std::string);
	void readConfig(std::string);
	void writeConfig(std::string);
	void insertRecord(std::string,std::string);
	std::string getRecord(std::string,int);
	long V(std::string tableName, std::string colName);
	
	static DBSystem *getInstance();
	
private:
	static DBSystem *singleton;
	DBSystem();
	bool canLineBeAdded(std::string,int); 
	
};

#endif /*DBSYSTEM_H_*/
