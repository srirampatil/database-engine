/*
 * QueryParser.cpp
 *
 *  Created on: 11-Feb-2014
 *      Author: sriram
 */

#include "QueryParser.hpp"
#include <iostream>
#include <core/gsp_base.h>
#include <set>

#include "expr_traverse.h"
#include "QueryExecutor.hpp"
#include "Utils.hpp"

#include <iostream>
#include <fstream>

#include "configurations.hpp"

#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/find.hpp>
#include <boost/range/iterator_range.hpp>
#include <algorithm>

#include "DBSystem.h"

using namespace std;

QueryParser::QueryParser() {
	if (gsp_parser_create(dbvmysql, &sqlParser)) {
		cerr << "Error creating parser: " << gsp_errmsg(sqlParser);
	}

	traverser = NULL;
	nodeList = NULL;
}

QueryParser::~QueryParser() {
	gsp_parser_free(sqlParser);
	traverser->dispose(traverser);
	nodeList->dispose(nodeList);
}

void QueryParser::replaceSpaces(std::string &query) {
	int i = 0, j = 0;
	int length = query.length();
	bool foundSpace = false;
	int quoteCount = 0;

	for (; i < length; i++) {
		if (query[i] != SPACE_CHAR) {
			foundSpace = false;
			query[j++] = query[i];

			/* Copying the quoted strings as it is */
			if (query[i] == SINGLE_QUOTE_CHAR || query[i] == DOUBLE_QUOTE_CHAR) {
				char startingChar = query[i++];
				while (i < length) {
					query[j++] = query[i];
					if (query[i] == startingChar)
						break;
					i++;
				}
			}

		} else if (!foundSpace) {
			foundSpace = true;
			j++;
		}
	}

	while (query[j - 1] == ';')
		j--;
	query[j] = '\0';

	query = Utils::trim(query);
}

char *QueryParser::printString(gsp_sourcetoken *startToken,
		gsp_sourcetoken *endToken, int offset) {

	if (strcmp(startToken->pStr, endToken->pStr) == 0) {
		char *clause = new char[endToken->nStrLen + 1 - offset];
		memcpy(clause, startToken->pStr + offset, startToken->nStrLen);
		clause[startToken->nStrLen - offset] = '\0';
		return clause;

	} else {
		char *clause = new char[endToken->nOffSet - startToken->nOffSet + 1
				- offset];
		memcpy(clause, startToken->pStr + startToken->nStrLen + offset + 1,
				endToken->nOffSet - startToken->nOffSet - offset);
		clause[endToken->nOffSet - startToken->nOffSet - offset] = '\0';
		return clause;
	}
}

void QueryParser::parseAttributes(vector<string> &listColumns,
		vector<string> &listDatatypes) {

	std::string s = attributesStr;
	std::string delimiter = ",";

	size_t pos = 0;
	std::string token;
	while ((pos = s.find(delimiter)) != std::string::npos) {
		token = s.substr(0, pos);

		string subString = token.substr(0, token.find_first_of(SPACE_CHAR));
		listColumns.push_back(Utils::trim(subString));

		subString = token.substr(token.find_first_of(SPACE_CHAR) + 1);
		listDatatypes.push_back(Utils::trim(subString));
		s.erase(0, pos + delimiter.length());
	}
	//cout << "s = " << s << endl;
	string subString = s.substr(0, s.find_first_of(SPACE_CHAR));
	listColumns.push_back(Utils::trim(subString));

	subString = s.substr(s.find_first_of(SPACE_CHAR) + 1);
	listDatatypes.push_back(Utils::trim(subString));

}

void parseColumns(vector<string> &listColumns, string columnStr,
		vector<string> listTables) {

	if (columnStr == "*") {

		for (unsigned int i = 0; i < listTables.size(); i++) {

			boost::trim(listTables[i]);

			std::map<std::string, std::string> eachTableColumnMap =
					Configurations::tables[listTables[i]];

			string fileName = Configurations::pathForData;
			fileName.append("/");
			fileName.append(listTables[i]);
			fileName.append(".data");

			ifstream dataReader(fileName.c_str(), ifstream::in);
			string schema;
			getline(dataReader, schema);
			dataReader.close();

			vector<string> splitStrings;
			boost::split(splitStrings, schema, boost::is_any_of(":,"));

			int size = splitStrings.size();
			for (int j = 0; j < size; j += 2) {
				boost::trim(splitStrings[i]);
				if (find(listColumns.begin(), listColumns.end(),
						splitStrings[j]) == listColumns.end())
					listColumns.push_back(splitStrings[j]);
			}

		}

		/*
		 *****************
		 * Never ever uncomment the code ...
		 *****************
		 for (unsigned int i = 0; i < listTables.size(); i++) {

		 std::map<std::string, std::string> eachTableColumnMap =
		 Configurations::tables[listTables[i]];

		 for (StringMapEntryIterator columnIterator =
		 eachTableColumnMap.begin(); columnIterator
		 != eachTableColumnMap.end(); columnIterator++) {
		 StringMapEntry column = *columnIterator;

		 //string colName = listTables[i];
		 //colName += ".";
		 string colName;
		 colName += column.first;

		 if (find(listColumns.begin(), listColumns.end(), colName)
		 == listColumns.end())
		 listColumns.push_back(colName);
		 }
		 }

		 */
		return;

	}

	std::string s = columnStr;
	std::string delimiter = ",";

	size_t pos = 0;
	std::string token;
	while ((pos = s.find(delimiter)) != std::string::npos) {
		token = s.substr(0, pos);
		//std::cout << token << std::endl;
		listColumns.push_back(Utils::trim(token));
		s.erase(0, pos + delimiter.length());
	}
	//cout << "s = " << s << endl;
	if (Utils::trim(s).size() != 0)
		listColumns.push_back(Utils::trim(s));
}

bool areAttributesValid(vector<string> &listColumns,
		vector<string> &listDatatypes) {

	for (unsigned int i = 0; i < listColumns.size(); i++) {

		if (listDatatypes[i] == TYPE_INTEGER) {
		} else if (listDatatypes[i] == TYPE_FLOAT) {
		} else {
			string temp = listDatatypes[i];
			if (boost::iequals(temp.substr(0, 7), TYPE_VARCHAR)) {
				// TODO: Empty implementation?
			} else {
				return false;
			}
		}
	}
	return true;
}

void createDataFile(string tableName, vector<string> listColumns,
		vector<string> listDatatypes) {

	ofstream dataFile;
	string tempFileName = Configurations::pathForData;
	tempFileName += "/";
	tempFileName.append(tableName);
	tempFileName.append(".data");
	dataFile.open(tempFileName.c_str(), ios::out);

	if (listColumns.size() == 0) {
		dataFile.close();
		return;
	}

	dataFile << listColumns[0] << ":" << listDatatypes[0];
	for (unsigned int i = 1; i < listColumns.size(); i++) {
		dataFile << "," << listColumns[i] << ":" << listDatatypes[i];
	}

	dataFile.close();
}

void createCSVFile(string tableName) {

	ofstream csvFile;
	string tempFileName = Configurations::pathForData;
	tempFileName += "/";
	tempFileName.append(tableName);
	tempFileName.append(".csv");
	csvFile.open(tempFileName.c_str(), ios::out);
	csvFile.close();
}

void QueryParser::init() {

	queryTypeStr.clear();
	//tablenameStr.clear();
	columnsStr.clear();
	distinctStr.clear();
	conditionStr.clear();
	orderbyStr.clear();
	groupbyStr.clear();
	havingStr.clear();
	attributesStr.clear();
	listOfTables.clear();
	joinConditionsVector.clear();
}

int parseOperator(string &conditionString, int index, string &op) {
	/* Checking the operator */
	if (conditionString[index] == OP_LESS_THAN
			|| conditionString[index] == OP_GREATER_THAN) {

		op.push_back(conditionString[index]);
		index++;

		if (conditionString[index] == OP_EQUAL_TO) {
			op.push_back(conditionString[index]);
			index++;
		}

	} else if (conditionString[index] == OP_EQUAL_TO) {
		op.push_back(conditionString[index]);
		index++;

	} else {
		string data = conditionString.substr(index, 4);
		std::transform(data.begin(), data.end(), data.begin(), ::toupper);

		if (data == OP_LIKE) {
			op = OP_LIKE;
			index += 4;

		} else
			op = "WRONG OP";
	}

	return index;
}

bool isWhereValid(string conditionString, vector<string> listOfTables) {

	unsigned int i = 0;
	while (i < conditionString.size()) {

		string lhs;
		string rhs;
		string op;

		bool isQuoteAppeared = false;
		for (; i < conditionString.size(); i++) {
			char c = conditionString[i];
			if ((isalpha(c) || isdigit(c) || c == DOT_CHAR
					|| (c == UNDERSCORE_CHAR) || c == HYPHEN_CHAR
					|| (c == SINGLE_QUOTE_CHAR) || (c == DOUBLE_QUOTE_CHAR))) {

				if ((c == DOUBLE_QUOTE_CHAR || c == SINGLE_QUOTE_CHAR)
						&& isQuoteAppeared == true) {
					lhs += c;
					i++;
					break;
				}
				if (c == DOUBLE_QUOTE_CHAR || c == SINGLE_QUOTE_CHAR)
					isQuoteAppeared = true;
				lhs += c;
			} else {
				if (isQuoteAppeared == false)
					break;
			}
		}

		if (conditionString[i] == SPACE_CHAR)
			i++;

		i = parseOperator(conditionString, i, op);

		if (conditionString[i] == SPACE_CHAR)
			i++;

		isQuoteAppeared = false;
		for (; i < conditionString.size(); i++) {
			char c = conditionString[i];
			if ((isalpha(c) || isdigit(c) || c == DOT_CHAR
					|| (c == UNDERSCORE_CHAR) || c == HYPHEN_CHAR
					|| (c == SINGLE_QUOTE_CHAR) || (c == DOUBLE_QUOTE_CHAR))) {

				if ((c == DOUBLE_QUOTE_CHAR || c == SINGLE_QUOTE_CHAR)
						&& isQuoteAppeared == true) {
					rhs += c;
					i++;
					break;
				}
				if (c == DOUBLE_QUOTE_CHAR || c == SINGLE_QUOTE_CHAR)
					isQuoteAppeared = true;
				rhs += c;
			} else {
				if (isQuoteAppeared == false)
					break;
			}
		}

		if (conditionString[i] == SPACE_CHAR)
			i++;

		//cout << "Condition string:" << &conditionString[i] << endl;

		string conjunction;
		for (; i < conditionString.size(); i++) {
			char c = conditionString[i];
			if ((isalpha(c))) {
				conjunction += c;
			} else
				break;
		}
		if (conditionString[i] == SPACE_CHAR)
			i++;

		if (lhs == rhs) {
			return true;
		} else {

			string tLhs = Utils::type(lhs, listOfTables);
			string tRhs = Utils::type(rhs, listOfTables);

			if (tLhs == "NULL" || tRhs == "NULL") {
				return false;
			}

			if (boost::iequals(tLhs, tRhs)
					|| (boost::iequals(tLhs.substr(0, 7), TYPE_VARCHAR)
							&& boost::iequals(tRhs.substr(0, 7), TYPE_VARCHAR)
							&& (op == OP_LIKE || op[0] == OP_EQUAL_TO))) {
				return true;

			} else {
				return false;
			}
		}

	}

	return true;
}

bool isGroupbyValid(string groupbyString, vector<string> listOfTables) {
	boost::trim(groupbyString);
	if (groupbyString.empty())
		return true;

	return Utils::isColumnPresentInTable(groupbyString, listOfTables);
}

bool isOrderbyValid(string orderbyString, vector<string> listOfTables) {
	if (Utils::trim(orderbyString) == "")
		return true;

	vector<string> listOfOrderBy;
	boost::split(listOfOrderBy, orderbyString, boost::is_any_of(","));

	for (vector<string>::iterator orderByColumn = listOfOrderBy.begin();
			orderByColumn != listOfOrderBy.end(); orderByColumn++) {

		if (Utils::isColumnPresentInTable(Utils::trim(*orderByColumn),
				listOfTables) == false)
			return false;
	}

	return true;
}

bool isHavingValid(string conditionString, vector<string> listOfTables) {
	if (Utils::trim(conditionString) == "")
		return true;

	unsigned int i = 0;
	while (i < conditionString.size()) {

		string lhs;
		string rhs;
		string op;

		bool isQuoteApprered = false;
		for (; i < conditionString.size(); i++) {
			char c = conditionString[i];
			if ((isalpha(c) || isdigit(c) || c == DOT_CHAR
					|| (c == UNDERSCORE_CHAR) || c == HYPHEN_CHAR
					|| (c == SINGLE_QUOTE_CHAR) || (c == DOUBLE_QUOTE_CHAR))) {

				if ((c == DOUBLE_QUOTE_CHAR || c == SINGLE_QUOTE_CHAR)
						&& isQuoteApprered == true) {
					lhs += c;
					i++;
					break;
				}
				if (c == DOUBLE_QUOTE_CHAR || c == SINGLE_QUOTE_CHAR)
					isQuoteApprered = true;
				lhs += c;
			} else {
				if (isQuoteApprered == false)
					break;
			}
		}
		if (conditionString[i] == SPACE_CHAR)
			i++;

		i = parseOperator(conditionString, i, op);

		if (conditionString[i] == SPACE_CHAR)
			i++;

		isQuoteApprered = false;
		for (; i < conditionString.size(); i++) {
			char c = conditionString[i];
			if ((isalpha(c) || isdigit(c) || c == DOT_CHAR
					|| (c == UNDERSCORE_CHAR) || c == HYPHEN_CHAR
					|| (c == SINGLE_QUOTE_CHAR) || (c == DOUBLE_QUOTE_CHAR))) {

				if ((c == DOUBLE_QUOTE_CHAR || c == SINGLE_QUOTE_CHAR)
						&& isQuoteApprered == true) {
					rhs += c;
					i++;
					break;
				}
				if (c == DOUBLE_QUOTE_CHAR || c == SINGLE_QUOTE_CHAR)
					isQuoteApprered = true;
				rhs += c;
			} else {
				if (isQuoteApprered == false)
					break;
			}
		}

		if (conditionString[i] == SPACE_CHAR)
			i++;

		string conjunction;
		for (; i < conditionString.size(); i++) {
			char c = conditionString[i];
			if ((isalpha(c))) {
				conjunction += c;
			} else
				break;
		}
		if (conditionString[i] == SPACE_CHAR)
			i++;

		if (lhs == rhs) {
			return true;
		} else {

			string tLhs = Utils::type(lhs, listOfTables);
			string tRhs = Utils::type(rhs, listOfTables);

			if (tLhs == "NULL" || tRhs == "NULL")
				return false;

			if (boost::equals(tLhs, tRhs) || (boost::iequals(tLhs.substr(0, 7),
			TYPE_VARCHAR) && boost::iequals(tRhs.substr(0, 7),
			TYPE_VARCHAR))) {
				return true;
			}
		}

	}

	return true;
}

void QueryParser::queryType(string query, bool canExecute) {
	init();
	replaceSpaces(query);

	char *cstr = new char[query.length() + 1];
	strcpy(cstr, query.c_str());
	if (gsp_check_syntax(sqlParser, cstr)) {
		cout << "A Query Invalid" << endl; //gsp_errmsg(sqlParser);
		return;
	}

	//	if(query[query.length() - 1] == ';')
	//		query.reserve(query.length() - 1);

	traverser = createSqlTraverser();
	nodeList = traverser->traverseSQL(traverser, &sqlParser->pStatement[0]);
	listIter = nodeList->getIterator(nodeList);
	if (nodeList->hasNext(nodeList, &listIter)) {
		gsp_node *node = (gsp_node *) nodeList->next(&listIter);
		gsp_sourcetoken *startToken = node->fragment.startToken;
		string queryStr = startToken->pStr;

		switch (node->nodeType) {
		case t_gsp_createTableStatement: {

			queryTypeStr = "create";
			string tablenameStr;

			char *tableName = startToken->pStr + 13;

			while (*tableName != LBRACKET_CHAR) {
				tablenameStr += *tableName++;
			}

			listOfTables.push_back(Utils::trim(tablenameStr));

			std::vector<std::string> listColumns;
			std::vector<std::string> listDatatypes;

			createCommand(query);
			parseAttributes(listColumns, listDatatypes);

			if (!areAttributesValid(listColumns, listDatatypes)
					|| Utils::isTablePresent(listOfTables[0])) {
				cout << "B Query Invalid" << endl;
				break;
			}

			cout << "Querytype:"
					<< ((queryTypeStr.length() == 0) ? "NA" : queryTypeStr)
					<< endl;

			cout << "Tablename:";
			cout << listOfTables[0];
			for (unsigned int i = 1; i < listOfTables.size(); i++) {
				cout << "," << listOfTables[i];
			}
			cout << endl;
			cout << "Attributes:";

			cout << listColumns[0] << ":" << listDatatypes[0];
			for (unsigned int i = 1; i < listColumns.size(); i++) {
				cout << "," << listColumns[i] << ":" << listDatatypes[i];
			}
			cout << endl;

			createDataFile(listOfTables[0], listColumns, listDatatypes);
			createCSVFile(listOfTables[0]);

			/* Make enty in system config file */

			string tableNameInsert = listOfTables[0];
			map<string, string> tableAttributes;

			for (unsigned int i = 0; i < listColumns.size(); i++) {
				string columnName = listColumns[i];
				string dataType = listDatatypes[i];
				tableAttributes.insert(StringMapEntry(columnName, dataType));
			}

			Configurations::tables.insert(
					TableMapEntry(tableNameInsert, tableAttributes));

			DBSystem *dbSystem = DBSystem::getInstance();
			dbSystem->writeConfig(Configurations::CONFIG_FILE_PATH);
			break;
		}

		case t_gsp_selectStatement: {

			queryTypeStr = "select";

			int i = startToken->nStrLen;
			int length = queryStr.length();

			string distinctStrTemp, columnStrTemp;
			bool isDistinct = false;

			while (1) {
				if (queryStr[i] == SPACE_CHAR || queryStr[i] == LBRACKET_CHAR
						|| queryStr[i] == RBRACKET_CHAR
						|| queryStr[i] == COMMA_CHAR) {
					i++;
					continue;
				}

				if ((i + 4) < length&& (queryStr.compare(i, 4, "from") == 0
						|| queryStr.compare(i, 4, "FROM") == 0)
				&& queryStr[i + 4] == SPACE_CHAR) {
					break;
				}

				if ((i + 8) < length
						&& (queryStr.compare(i, 8, "distinct") == 0
								|| queryStr.compare(i, 8, "DISTINCT") == 0)
						&& (queryStr[i + 8] == SPACE_CHAR
								|| queryStr[i + 8] == LBRACKET_CHAR)) {
					if (!distinctStrTemp.empty())
						distinctStrTemp += ",";

					if (!columnStrTemp.empty())
						columnStrTemp += ",";

					i += 9;
					isDistinct = true;

				} else {
					if (!columnStrTemp.empty())
						columnStrTemp += ",";

					isDistinct = false;
				}

				while (queryStr[i] != RBRACKET_CHAR && queryStr[i] != SPACE_CHAR
						&& queryStr[i] != COMMA_CHAR) {

					columnStrTemp += queryStr[i];
					if (isDistinct)
						distinctStrTemp += queryStr[i];

					i++;
				}
			}

			columnsStr = Utils::trim(columnStrTemp);
			distinctStr = Utils::trim(distinctStrTemp);

			vector<string> listColumns;
			selectCommand(query);

			bool isValid = true;

			/* Table Existence Check */
			for (unsigned int i = 0; i < listOfTables.size(); i++) {
				if (!Utils::isTablePresent(listOfTables[i])) {
					cout << "Table : " << listOfTables[i] << " do not exist"
							<< endl;
					isValid = false;
					break;
				}
			}

			if (isValid)
				parseColumns(listColumns, columnsStr, listOfTables);

			/* Checking for column */
			for (unsigned int i = 0;
					columnsStr != "*" && isValid && i < listColumns.size();
					i++) {
				if (!Utils::isColumnPresentInTable(listColumns[i],
						listOfTables)) {
					isValid = false;
					break;
				}
			}

			if (isValid && !isWhereValid(conditionStr, listOfTables)) {
				isValid = false;
			}

			if (isValid && !isHavingValid(havingStr, listOfTables)) {
				isValid = false;
			}

			if (isValid && !isGroupbyValid(groupbyStr, listOfTables)) {
				isValid = false;
			}

			if (isValid && !isOrderbyValid(orderbyStr, listOfTables)) {
				isValid = false;
			}

			if (!isValid) {
				cout << "Query Invalid" << endl;
				break;
			}

			if (canExecute == false) {
				cout << "Querytype: "
						<< (queryTypeStr.length() == 0 ? "NA" : queryTypeStr)
						<< endl;

				cout << "Tablename: ";
				cout << listOfTables[0];
				for (unsigned int i = 1; i < listOfTables.size(); i++) {
					cout << "," << listOfTables[i];
				}
				cout << endl;

				cout << "Columns: ";
				cout << listColumns[0];
				for (unsigned int i = 1; i < listColumns.size(); i++) {
					cout << "," << listColumns[i];
				}
				cout << endl;

				cout << "Distinct: "
						<< (distinctStr.length() == 0 ? "NA" : distinctStr)
						<< endl;
				cout << "Condition: "
						<< (conditionStr.length() == 0 ? "NA" : conditionStr)
						<< endl;
				cout << "Orderby: "
						<< (orderbyStr.length() == 0 ? "NA" : orderbyStr)
						<< endl;
				cout << "Groupby: "
						<< (groupbyStr.length() == 0 ? "NA" : groupbyStr)
						<< endl;
				cout << "Having: "
						<< (havingStr.length() == 0 ? "NA" : havingStr) << endl;

				cout << "JOIN: ";
				for(unsigned int i = 0; i < joinConditionsVector.size(); i++) {
					if(i > 0)
						cout << COMMA_CHAR << " ";

					cout << joinConditionsVector[i];
				}

				cout << endl;

				break;
			}

			vector<string> conditionsVector;
			vector<string> orderByVector;
			boost::split(conditionsVector, conditionStr, boost::is_any_of(","));
			boost::split(orderByVector, orderbyStr, boost::is_any_of(","));

			int vecSize = orderByVector.size();
			for (int i = 0; i < vecSize; i++)
				Utils::trim(orderByVector[i]);

			StringVectorMap paramsMap;

			paramsMap.insert(
					StringVectorMapEntry(QueryExecutor::TABLES, listOfTables));
			paramsMap.insert(
					StringVectorMapEntry(QueryExecutor::COLUMNS, listColumns));
			paramsMap.insert(
					StringVectorMapEntry(QueryExecutor::CONDITIONS,
							conditionsVector));
			paramsMap.insert(
					StringVectorMapEntry(QueryExecutor::ORDER_BY,
							orderByVector));
			paramsMap.insert(
					StringVectorMapEntry(QueryExecutor::JOIN,
							joinConditionsVector));

			if (canExecute) {
				QueryExecutor *executor = new QueryExecutor();
				executor->execute(paramsMap);
			}

			break;
		}

		default:
			break;
		}
	}
}

void QueryParser::createCommand(string query) {

	while (nodeList->hasNext(nodeList, &listIter)) {
		gsp_node *node = (gsp_node *) nodeList->next(&listIter);
		gsp_sourcetoken *startToken = node->fragment.startToken;
		gsp_sourcetoken *endToken = node->fragment.endToken;
		string queryStr = startToken->pStr;

		//TODO: test if the code fails for a non existent data type is given to a column.

		switch (node->nodeType) {
		case t_gsp_columnDefinition: {
			char *end = strstr(startToken->pStr, node->fragment.endToken->pStr);
			if (end != NULL) {
				if (!attributesStr.empty())
					attributesStr += (COMMA_CHAR);

				bool hasOpeningBracket = false;
				char *ptr = startToken->pStr;
				while (ptr != end) {
					if (*ptr == LBRACKET_CHAR)
						hasOpeningBracket = true;
					attributesStr += (*ptr++);
				}

				for (int i = 0; i < node->fragment.endToken->nStrLen; i++)
					attributesStr += (*end++);

				if (hasOpeningBracket && *end == RBRACKET_CHAR)
					attributesStr += (*end++);
			}
			attributesStr = Utils::trim(attributesStr);
			break;
		}

		default:
			break;
		}
	}
}

void QueryParser::selectCommand(string query) {

	set<string> setOfTableNames;

	while (nodeList->hasNext(nodeList, &listIter)) {
		gsp_node *node = (gsp_node *) nodeList->next(&listIter);
		gsp_sourcetoken *startToken = node->fragment.startToken;
		string queryStr = startToken->pStr;
		string tableNameStr;

		string joinStr;
		string joinConditionStr;
		int joinConditionIndex = 0;

		switch (node->nodeType) {
		case t_gsp_whereClause:

			conditionStr = (const char *) printString(node->fragment.startToken,
					node->fragment.endToken, 0);

			conditionStr = Utils::trim(conditionStr);
			break;

		case t_gsp_fromTable:

			tableNameStr = (const char *) printString(node->fragment.startToken,
					node->fragment.endToken, 0);

			boost::trim(tableNameStr);
			if (boost::iequals(tableNameStr.substr(0, 10), "inner join")
					|| boost::iequals(tableNameStr.substr(0, 5), "join ")) {

			} else {
				listOfTables.push_back(Utils::trim(tableNameStr));
			}
			break;

		case t_gsp_joinExpr: {

			joinStr = (const char *) printString(node->fragment.startToken,
					node->fragment.endToken, 0);

			boost::iterator_range<string::iterator> result = boost::ifind_last(joinStr, "JOIN ");
			joinStr = joinStr.substr(result.begin() - joinStr.begin());

			joinConditionIndex = 0;
			while ((joinStr[joinConditionIndex] != 'o'
					&& joinStr[joinConditionIndex] != 'O')
					|| (joinStr[(joinConditionIndex + 1)] != 'n'
							&& joinStr[(joinConditionIndex + 1)] != 'N')
					|| (joinStr[(joinConditionIndex + 2)] != SPACE_CHAR))
				joinConditionIndex++;


			bool conditionFound = false;
			bool foundOper = false;
			unsigned int joinConditionEnd = joinConditionIndex + 3;
			while(joinConditionEnd < joinStr.length()) {
				switch(joinStr[joinConditionEnd]) {
				case '=':
					foundOper = true;
					if(joinStr[joinConditionEnd + 1] == ' ')
						joinConditionEnd++;
					break;

				case ' ':
					if(foundOper)
						conditionFound = true;
					break;
				}

				if(conditionFound)
					break;

				joinConditionEnd++;
			}

			joinConditionStr = joinStr.substr(joinConditionIndex + 3,
					joinConditionEnd - joinConditionIndex - 3);
			boost::trim(joinConditionStr);
			joinConditionsVector.push_back(joinConditionStr);

			break;
		}

		case t_gsp_orderBy:

			orderbyStr = (const char *) printString(node->fragment.startToken,
					node->fragment.endToken, 3);
			orderbyStr = Utils::trim(orderbyStr);
			cout << "OrderBy obtained : " << orderbyStr << endl;
			break;

		case t_gsp_groupBy: {
			queryStr = printString(node->fragment.startToken,
					node->fragment.endToken, 3);

			int length = queryStr.length();

			string havingStrTemp, groupByStrTemp;
			string *currentStrPtr;

			int i = 0;
			bool isHavingAppeared = false;
			while (i < length) {
				if (queryStr[i] == SPACE_CHAR || queryStr[i] == LBRACKET_CHAR
						|| queryStr[i] == RBRACKET_CHAR
						|| queryStr[i] == COMMA_CHAR) {
					i++;
					continue;
				}

				if ((i + 6) < length
						&& (queryStr.compare(i, 6, "having") == 0
								|| queryStr.compare(i, 6, "HAVING") == 0)
						&& (queryStr[i + 6] == SPACE_CHAR
								|| queryStr[i + 6] == LBRACKET_CHAR)) {
					if (!havingStrTemp.empty())
						havingStrTemp += ",";

					i += 7;
					currentStrPtr = &havingStrTemp;
					isHavingAppeared = true;

				} else {
					if (isHavingAppeared == false) {
						if (!groupByStrTemp.empty())
							groupByStrTemp += ",";
						currentStrPtr = &groupByStrTemp;
					}
				}

				while (i < length && queryStr[i] != RBRACKET_CHAR
						&& queryStr[i] != SPACE_CHAR
						&& queryStr[i] != COMMA_CHAR)
					(*currentStrPtr) += queryStr[i++];
			}

			groupbyStr = Utils::trim(groupByStrTemp);
			havingStr = Utils::trim(havingStrTemp);

			break;
		}

		default:
			break;
		}
	}
}
