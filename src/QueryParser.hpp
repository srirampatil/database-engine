/*
 * QueryParser.hpp
 *
 *  Created on: 11-Feb-2014
 *      Author: sriram
 */

#ifndef QUERYPARSER_HPP_
#define QUERYPARSER_HPP_

#include <core/gsp_sqlparser.h>
#include <ext/collection/includes/linked_list.h>
#include <string>
#include "node_visitor.h"
#include "dbconstants.hpp"
#include <vector>

class QueryParser {
private:
	gsp_sqlparser *sqlParser;

	SqlTraverser *traverser;
	List *nodeList;
	Iterator listIter;

	char *printString(gsp_sourcetoken *startToken, gsp_sourcetoken *endToken,
			int offset);

	void parseAttributes(std::vector<std::string> &listColumns,
			std::vector<std::string> &listDatatypes);
	void replaceSpaces(std::string &query);
	void init();

	/* Select Table */
	std::string queryTypeStr;
	//std::string tablenameStr;
	std::string columnsStr;
	std::string distinctStr;
	std::string conditionStr;
	std::string orderbyStr;
	std::string groupbyStr;
	std::string havingStr;
	std::vector<std::string> joinConditionsVector;

	/* Create Table */
	std::string attributesStr;
	
	std::vector< std::string > listOfTables;

public:
	QueryParser();
	virtual ~QueryParser();

	void queryType(std::string query , bool canExecute);
	void createCommand(std::string query);
	void selectCommand(std::string query);
};

#endif /* QUERYPARSER_HPP_ */

