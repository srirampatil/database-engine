#include <iostream>
#include <fstream>
#include <sstream>
#include <boost/algorithm/string.hpp>

#include "Utils.hpp"
#include "configurations.hpp"

using namespace std;

string Utils::toString(int a) {
	stringstream ss;
	ss << a;
	return ss.str();
}

string Utils::trim(string &s) {
	boost::algorithm::trim(s);
	return s;
}

bool Utils::isTablePresent(string tableName) {
	return Configurations::tables.find(tableName)
			!= Configurations::tables.end();
}

/**
 * Get the common columns between any two tables
 *
 * @param table1
 * @param table2
 * @return list of commmon column names
 */
list<string> Utils::commonColumnsBetween(string table1, string table2) {
	list<string> columnsList;
	if (!isTablePresent(table1) || !isTablePresent(table2))
		return columnsList;

	map<string, string> table1ColumnsMap = Configurations::tables.find(table1)->second;

	map<string, string> table2ColumnsMap = Configurations::tables.find(table2)->second;

	map<string, string>::iterator table1MapIt = table1ColumnsMap.begin();
	while (table1MapIt != table1ColumnsMap.end()) {
		if (table2ColumnsMap.find(table1MapIt->first) != table2ColumnsMap.end())
			columnsList.push_back(table1MapIt->first);

		++table1MapIt;
	}

	return columnsList;
}

/**
 * Returns the index of column from the schema file
 * @author: Sriram
 * 
 * @colName : columne name to be searched
 * @tableName : name of the table
 * 
 * @return index of the column name from schema file
 */
int Utils::columnIndexInTable(std::string colName, std::string tableName) {
	/* Check if the table is present. */
	if (!isTablePresent(tableName))
		return -1;

	string schemaFilePath = Configurations::pathForData + "/" + tableName
			+ ".data";

	ifstream schemaFileStream(schemaFilePath.c_str());
	string schemaStr;
	getline(schemaFileStream, schemaStr);
	schemaFileStream.close();

	/* The columns are separated by comma */
	vector<string> columnsAndTypeVector;
	boost::split(columnsAndTypeVector, schemaStr, boost::is_any_of(","));

	for (int i = 0; i < columnsAndTypeVector.size(); i++) {
		/* Each column has columnName:dataType. So, splitting to compare column names */
		std::size_t index = columnsAndTypeVector[i].find_first_of(COLON_CHAR,
		0);
		if (colName == columnsAndTypeVector[i].substr(0, index))
			return i;
	}

	return -1;
}

bool Utils::isColumnPresentInTable(string columnName, vector<string> listTables) {
	std::size_t index = columnName.find_first_of(DOT_CHAR, 0);
	if (index == string::npos) {
		// . not present then do nothing
	} else {
		// .present then check for the column in table given
		string tableName = columnName.substr(0, index);
		columnName.erase(0, index + 1);

		if (!Utils::isTablePresent(tableName) || (find(listTables.begin(),
				listTables.end(), tableName) == listTables.end())) {
			return false;
		} else
			return true;

	}

	// Bug: this should be changed. It checks for the column in all the 
	// tables even if specific table is mentioned. Returns true of column is present in any table
	int count = 0;
	for (unsigned int i = 0; i < listTables.size(); i++) {
		std::map<std::string, std::string> eachTableColumnMap =
				Configurations::tables.find(listTables[i])->second;

		if (eachTableColumnMap.find(columnName) != eachTableColumnMap.end()) {
			return true;
		}
	}

	return false;
}

bool isInt(string s) {
	return !s.empty() && (s.find_first_not_of("+-0123456789") == string::npos);
}

bool isFloat(string s) {
	return !s.empty() && (s.find_first_not_of("+-0123456789.") == string::npos);
}

bool isVarchar(string s) {
	return !s.empty() && (s.find_first_of("'\"") != string::npos);
}

string Utils::type(string s, vector<string> listOfTables) {

	if (isInt(s)) {
		return TYPE_INTEGER;
	}

	if (isFloat(s)) {
		return TYPE_FLOAT;
	}

	if (isVarchar(s)) {
		return TYPE_VARCHAR;
	}

	if (isColumnPresentInTable(s, listOfTables)) {
		std::size_t index = s.find_first_of(DOT_CHAR, 0);
		string tableName = listOfTables[0];
		if (index == string::npos) {

		} else {
			// .present then check for the column in table given
			tableName = s.substr(0, index);
			s.erase(0, index + 1);
		}

		std::map<std::string, std::string> eachTableColumnMap =
				Configurations::tables.find(tableName)->second;
		return (eachTableColumnMap)[s];

	} else {
		return "NULL";
	}
	return "NULL";
}

string Utils::columnTypeInTable(string columnName, string tableName) {

	std::map<std::string, std::string> eachTableColumnMap =
			Configurations::tables.find(tableName)->second;
	return (eachTableColumnMap)[columnName];

}

void Utils::create_dense_index(std::string table_name, std::string index_key) {
	string fileName = Configurations::pathForData;
	fileName.append("/");
	fileName.append(table_name);
	fileName.append(".data");

	ifstream dataReader(fileName.c_str(), ifstream::in);
	string schema;
	getline(dataReader, schema);
	dataReader.close();

	vector<string> splitStrings;
	boost::split(splitStrings, schema, boost::is_any_of(":,"));

	boost::trim(index_key);

	int size = splitStrings.size();
	int i, index = 0;
	for (i = 0; i < size; i += 2) {

		boost::trim(splitStrings[i]);
		if (index_key == splitStrings[i]) {
			break;
		}

		index++;
	}

	// index is the index of the primary key column in the CSV file

	int currentLineNumber = 0;
	string csvFilePath = Configurations::pathForData + "/" + table_name
			+ ".csv";

	ifstream csvFileStream(csvFilePath.c_str());

	string indexfileName = "/tmp/";
	indexfileName.append(table_name);
	indexfileName.append("_pk");
	indexfileName.append(".idx");

	ofstream indexFileStream;

	indexFileStream.open(indexfileName.c_str(), ios::out);

	std::string lineRead;

	while (!csvFileStream.eof()) {
		getline(csvFileStream, lineRead);
		if (lineRead.empty())
			continue;

		vector<string> splitVector;
		boost::split(splitVector, lineRead, boost::is_any_of(","));
		boost::replace_all(splitVector[index], "\"", "");
		indexFileStream << splitVector[index] << ":" << currentLineNumber
				<< endl;

		currentLineNumber++;
	}

	indexFileStream.close();
	csvFileStream.close();

}
