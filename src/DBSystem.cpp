/*
 * DBSystem.cpp
 *
 *  Created on: 25-Jan-2014
 *      Author: arpit, sriram
 */

#include "DBSystem.h"

#include "mem_model.hpp"
#include "parser.hpp"
#include "configurations.hpp"
#include "QueryParser.hpp"
#include "Utils.hpp"

#include <boost/algorithm/string.hpp>
#include <iostream>
#include <fstream>
#include <set>

using namespace std;

DBSystem * DBSystem::singleton = NULL;

DBSystem::DBSystem() {
}

DBSystem::~DBSystem() {
}

DBSystem * DBSystem::getInstance() {
	if (singleton == NULL)
		singleton = new DBSystem();

	return singleton;
}

bool DBSystem::canLineBeAdded(string tableName, int length) {
	return true;
}

void DBSystem::writeConfig(string filePath) {

	ofstream dataFile;
	dataFile.open(filePath.c_str(), ios::out);

	dataFile << Configurations::TAG_PAGESIZE << " " << Configurations::pageSize
			<< endl;
	dataFile << Configurations::TAG_NUM_PAGES << " "
			<< Configurations::numberOfPages << endl;
	dataFile << Configurations::TAG_PATH_FOR_DATA << " "
			<< Configurations::pathForData << endl;

	for (TableMapIterator eachTable = Configurations::tables.begin();
			eachTable != Configurations::tables.end(); eachTable++) {
		TableMapEntry ent = *eachTable;
		dataFile << Configurations::TAG_BEGIN_TABLE << endl;
		dataFile << ent.first << endl;
		std::map<std::string, std::string> eachTableColumnMap = ent.second;
		for (StringMapEntryIterator columnIterator = eachTableColumnMap.begin();
				columnIterator != eachTableColumnMap.end(); columnIterator++) {
			StringMapEntry column = *columnIterator;
			dataFile << column.first << "," << column.second << endl;
		}
		dataFile << Configurations::TAG_END_TABLE << endl;
	}

	dataFile.close();
}

void DBSystem::readConfig(string filePath) {
	ConfigParser *parser = ParserFactory::newParserInstance();
	parser->parse(filePath);
}

string DBSystem::getRecord(string tableName, int recordId) {

	MemoryModel *mm = MemoryModel::getInstance(Configurations::numberOfPages,
			Configurations::pageSize);

	TableBlockMapIterator tableBlockIterator = Configurations::tableBlocks.find(
			tableName);
	BlockLineNumberMap m = (BlockLineNumberMap) tableBlockIterator->second;
	BlockLineNumberMapIterator blockLineItr = m.begin();

	blockLineItr = m.begin();

	int previousPageNumber = blockLineItr->first;
	int blockNumberToGetIntoMemory = -1;

	LineOffset *currentLineOffset = blockLineItr->second;

	if (currentLineOffset != NULL
			&& recordId == currentLineOffset->lineNumber) {
		blockNumberToGetIntoMemory = previousPageNumber;
	}

	for ((blockLineItr == m.end()) ? blockLineItr : blockLineItr++;
			blockNumberToGetIntoMemory == -1 && blockLineItr != m.end();
			blockLineItr++) {

		int currentPageNumber = blockLineItr->first;
		currentLineOffset = blockLineItr->second;

		if (recordId == currentLineOffset->lineNumber) {
			blockNumberToGetIntoMemory = blockLineItr->first;
		} else if (recordId < currentLineOffset->lineNumber) {
			blockNumberToGetIntoMemory = previousPageNumber;
		} else {
			previousPageNumber = currentPageNumber;
		}
	}

	string filePath = Configurations::pathForData + "/" + tableName + ".csv";
	string line;

	if (blockNumberToGetIntoMemory == -1) {
		ifstream dFile(filePath.c_str());

		currentLineOffset = tableBlockIterator->second[previousPageNumber];
		dFile.seekg(currentLineOffset->fileOffset, dFile.beg);

		long lineNumber = 0;
		while (!dFile.eof()) {
			getline(dFile, line);

			if (recordId == lineNumber && !Utils::trim(line).empty()) {
				blockNumberToGetIntoMemory = previousPageNumber;
				break;
			}

			lineNumber++;
		}

		dFile.close();

		if (blockNumberToGetIntoMemory == -1)
			return "NULL";
	}

	TableBlockId * tableBlockId = new TableBlockId(tableName,
			blockNumberToGetIntoMemory);
	if (!mm->isPresent(tableBlockId)) {

		//cout << "Input to file : " << filePath << endl;
		ifstream dataFile(filePath.c_str());

		currentLineOffset =
				tableBlockIterator->second[blockNumberToGetIntoMemory];

		std::string line;

		dataFile.seekg(currentLineOffset->fileOffset, dataFile.beg);
		getline(dataFile, line);

		string blockDataString(line + "\n");

		if (m.find(blockNumberToGetIntoMemory + 1) != m.end()) {

			LineOffset *nextLineOffset =
					tableBlockIterator->second[blockNumberToGetIntoMemory + 1];

			int currentLineNumber = currentLineOffset->lineNumber;

			while (!dataFile.eof()
					&& currentLineNumber != (nextLineOffset->lineNumber - 1)) {
				getline(dataFile, line);
				if (line.empty())
					continue;

				blockDataString.append(line + "\n");
				currentLineNumber++;
			}

		} else {
			while (!dataFile.eof()) {
				getline(dataFile, line);
				if (line.empty())
					continue;

				blockDataString.append(line + "\n");
			}
		}

		if (!blockDataString.empty())
			blockDataString.resize(blockDataString.length() - 1);
		int result = mm->allocate(tableBlockId, blockDataString.c_str(),
				blockDataString.length());

		dataFile.close();

		/* switch (result) {
		 case PAGE_SIZE_EXCEEDED:
		 return "";
		 case PAGE_NOT_REPLACED:
		 cout << "HIT" << endl;
		 break;
		 default:
		 cout << "MISS " << result <<currentLineOffset endl;
		 break;
		 } */
	} else {
//		cout << "HIT" << endl;
		mm->touch(tableBlockId);
	}

	//mm->traverseList();

	return mm->getRecordForBlock(tableBlockId, recordId,
			tableBlockIterator->second[blockNumberToGetIntoMemory]->lineNumber);
}

void DBSystem::insertRecord(string tableName, string recordStr) {

	//cout << "Insert : " << tableName << " and record : " << record << endl;
	MemoryModel *mm = MemoryModel::getInstance(Configurations::numberOfPages,
			Configurations::pageSize);

	string filePath = Configurations::pathForData + "/" + tableName + ".csv";

	//cout << "Ouput to file : " << filePath << endl;
	std::ofstream outfile;
	outfile.open(filePath.c_str(), std::ios_base::app);
	outfile << recordStr << "\n";
	outfile.flush();
	outfile.close();

	TableBlockMapIterator tableBlockIterator = Configurations::tableBlocks.find(
			tableName);

	Configurations::tableBlocks.erase(tableBlockIterator);
	populateDBInfo(tableName);

	tableBlockIterator = Configurations::tableBlocks.find(tableName);
	int lastBlockNumber = Configurations::tableBlocks[tableName].size() - 1;

	//cout << "Last block of table is numbered : " << lastBlockNumber << endl;
	//cout << "Start line numb " << blockStartLineNumber << endl;

	TableBlockId * tableBlockId = new TableBlockId(tableName, lastBlockNumber);

	if (mm->isPresent(tableBlockId)) {
		//cout << "Block already present in memory." << endl;
		string blockData = mm->getBlockData(tableBlockId);
		mm->allocate(tableBlockId, blockData.c_str(), blockData.length());
	} else {
		//cout << "Block not present in memory." << endl;
		// get the last block from file create the block of data and then allocate

		LineOffset *blockLineOffset =
				tableBlockIterator->second[lastBlockNumber];

		ifstream dataFile(filePath.c_str());

		std::string line;

		//Skipping all the lines before the required block
		dataFile.seekg(blockLineOffset->fileOffset, dataFile.beg);
		getline(dataFile, line);

		string blockDataString(line + "\n");

		LineOffset *endLineOffset = tableBlockIterator->second[lastBlockNumber
				+ 1];

		while (endLineOffset != NULL
				&& blockLineOffset->lineNumber
						!= (endLineOffset->lineNumber - 1)) {
			getline(dataFile, line);
			blockDataString.append(line + "\n");
		}

		if (!blockDataString.empty())
			blockDataString.resize(blockDataString.length() - 1);

		//cout << "Block to be allocated " << blockDataString << endl;
		//cout << "Block to be allocated - length " << blockDataString.length() << endl;

		dataFile.close();

		//mm->touch(new TableBlockId(tableName , lastBlockNumber-1));
		mm->allocate(tableBlockId, blockDataString.c_str(),
				blockDataString.length());

	}

	Configurations::getTableDataForTable(tableName)->processNewRecord(recordStr);

	//cout << "Insert : " ;
	//mm->traverseList();

}

void DBSystem::populateDBInfo(string tableName) {

	string filePath = Configurations::pathForData + "/" + tableName + ".csv";

	int count_of_bytes = 0;
	int current_block_number = 0;
	int current_line_number = 0;

	map<int, LineOffset *> tableBlockLineNumberMap;

	ifstream file(filePath.c_str());
	string str;
	int countNewLine = 1;
	long offset = 0;

	while (std::getline(file, str)) {
		int lineLength = str.length();

		// If counting newline make record go to next page and removing it make it get appended the current page
		// then include the newline.
		// otherwise exclude it.
		if ((count_of_bytes + lineLength + countNewLine)
				> Configurations::pageSize
				&& (count_of_bytes + lineLength - countNewLine)
						<= Configurations::pageSize) {
			//TODO: fix this weird assignment
			//count = count;
		} else {
			lineLength += countNewLine;
		}

		//int count = str.length() + 1;
		/*int count = str.length();*/

		if ((count_of_bytes + lineLength) <= Configurations::pageSize) {
			if (tableBlockLineNumberMap.find(current_block_number)
					== tableBlockLineNumberMap.end()) {
				tableBlockLineNumberMap[current_block_number] = new LineOffset(
						current_line_number, offset);
			}

			count_of_bytes += lineLength;

		} else {
			current_block_number++;
			count_of_bytes = lineLength;
			tableBlockLineNumberMap[current_block_number] = new LineOffset(
					current_line_number, offset);
		}

		current_line_number++;
		offset += strlen(str.c_str()) + 1;
	}

	Configurations::tableBlocks[tableName] = tableBlockLineNumberMap;

	Configurations::readTableData(tableName);
}

void DBSystem::populateDBInfo() {

	for (TableMapIterator tableMapEntry = Configurations::tables.begin();
			tableMapEntry != Configurations::tables.end(); tableMapEntry++) {

		populateDBInfo((string) tableMapEntry->first);
	}

	/*for (TableBlockMapIterator iterator = Configurations::tableBlocks.begin() ; iterator
	 != Configurations::tableBlocks.end() ; iterator++) {
	 cout << "Table Name : " << iterator->first << endl;
	 map<int,int> m = iterator->second;
	 for (map<int,int>::iterator itr = m.begin() ; itr != m.end() ; itr++) {
	 cout << "Block number : " << itr->first << " line number : "
	 << itr->second << endl;
	 }
	 }*/
}

long DBSystem::V(std::string tableName, std::string colName) {

	Table *tableData = Configurations::getTableDataForTable(tableName);
	if(tableData == NULL)
		return -1;

	return tableData->totalDistinctValues(colName);
}

int main(int argc, char *argv[]) {

	Configurations::CONFIG_FILE_PATH = argv[1];

	/**
	 * 	Phase 1
	 */

	/* FILE *f;
	 f = fopen("/tmp/testcase", "r");
	 char *line= NULL;
	 int read;
	 DBSystem *dbs = DBSystem::getInstance();
	 dbs->readConfig(Configurations::CONFIG_FILE_PATH);
	 dbs->populateDBInfo();
	 unsigned long int len = 0;
	 while ((read = getline(&line, &len, f)) != -1) {
	 dbs->getRecord("op", atoi(line));
	 } */

	/**
	 * Phase 3
	 */

	//	DBSystem *dbSystem = DBSystem::getInstance();
	//	dbSystem->readConfig(Configurations::CONFIG_FILE_PATH);
	//	dbSystem->populateDBInfo();
	//
	//	QueryParser *qParser = new QueryParser();
	/* int t;
	 cin >> t;
	 string line;
	 t++;

	 while (t--) {
	 getline(cin, line);
	 if (line != "") {
	 qParser->queryType(line);
	 cout << endl;
	 }
	 } */

	/**
	 * Phase 2
	 */

	//	string line;
	//	 while (getline(cin, line)) {
	//	 if (line.compare("q") == 0 || line.compare("Q") == 0)
	//	 break;
	//
	//	 qParser->queryType(line);
	//	 }
	/**
	 * Phase 4 Part B
	 */

	/*DBSystem *dbSystem = DBSystem::getInstance();
	 dbSystem->readConfig(Configurations::CONFIG_FILE_PATH);
	 dbSystem->populateDBInfo();

	 dbSystem->getRecord("employee", 0);
	 dbSystem->getRecord("employee", 5);
	 dbSystem->getRecord("employee", 6);
	 dbSystem->getRecord("employee", 1);
	 dbSystem->getRecord("employee", 2);
	 dbSystem->getRecord("employee", 2);
	 dbSystem->getRecord("employee", 3);
	 dbSystem->getRecord("employee", 8);
	 dbSystem->getRecord("employee", 9);
	 dbSystem->getRecord("employee", 10);
	 dbSystem->getRecord("employee", 11);
	 dbSystem->getRecord("employee", 10);

	 dbSystem->V("employee", "age");
	 */

	/* dbSystem->getRecord("countries", 9);
	 dbSystem->getRecord("countries", 39);
	 dbSystem->getRecord("countries", 28);
	 dbSystem->getRecord("countries", 1);
	 dbSystem->getRecord("countries", 30);
	 dbSystem->getRecord("countries", 38);
	 dbSystem->getRecord("countries", 39);
	 dbSystem->getRecord("countries", 31);
	 dbSystem->insertRecord("countries", "record");
	 dbSystem->getRecord("countries", 42);
	 dbSystem->getRecord("countries", 28); */

	//cout << "Student" << endl;
	/*dbSystem->getRecord("student", 0);
	 dbSystem->getRecord("student", 1);
	 dbSystem->getRecord("student", 2);
	 dbSystem->getRecord("student", 3);
	 dbSystem->insertRecord("student", "record");
	 dbSystem->getRecord("student", 4);
	 dbSystem->insertRecord("student", "record1");
	 dbSystem->getRecord("student", 5);
	 */

	/**
	 * Phase 4 - Part A
	 */

	DBSystem *dbSystem = DBSystem::getInstance();
	dbSystem->readConfig(Configurations::CONFIG_FILE_PATH);
	dbSystem->populateDBInfo();

	int t;
	cin >> t;
	string line;
	t++;

	QueryParser *qParser = new QueryParser();

	while (t--) {
		getline(cin, line);
		if (line != "") {
			qParser->queryType(line, true);
			cout << endl;
		}
	}

	return 0;
}
