/*
 * configurations.hpp
 *
 *  Created on: 25-Jan-2014
 *      Author: sriram
 */

#ifndef CONFIGURATIONS_HPP_
#define CONFIGURATIONS_HPP_

#include <map>
#include <list>
#include <vector>
#include <set>
#include "mem_model.hpp"

struct LineOffset;

/*
 * TableName -> { AttributeName , Attribute DataType }
 */
typedef std::map<std::string, std::map<std::string, std::string> > TableMap;
typedef std::map<std::string, std::map<std::string, std::string> >::iterator TableMapIterator;
typedef std::pair<std::string, std::string> StringMapEntry;
typedef std::pair<std::string, std::map<std::string, std::string> > TableMapEntry;
typedef std::map<std::string, std::string>::iterator StringMapEntryIterator;

typedef std::map<std::string, std::map< int,LineOffset *> > TableBlockMap;
typedef std::map<std::string, std::map< int,LineOffset *> >::iterator TableBlockMapIterator;

typedef std::map<int,LineOffset *> BlockLineNumberMap;
typedef std::map<int,LineOffset *>::iterator BlockLineNumberMapIterator;

class Table {
private:
	std::string tableName;
	long totalRecords;
	std::vector<std::string> columnNamesVector;
	std::vector<std::set<std::string> *> columnValuesSetVector;


	void readColumns();
	void readDistinctRecordValues();

public:
	std::map<std::string, unsigned> columnIndexMap;

	std::string getTableName();
	long getTotalRecords();
	Table(std::string name);
	long totalDistinctValues(std::string columnName);
	void processNewRecord(std::string recordStr);
};

class Configurations {
private:
	static std::map<std::string, Table *> tableDataMap;

public:
	static std::string CONFIG_FILE_PATH;

	static const std::string TAG_PAGESIZE;
	static const std::string TAG_NUM_PAGES;
	static const std::string TAG_PATH_FOR_DATA;
	static const std::string TAG_BEGIN_TABLE;
	static const std::string TAG_END_TABLE;
	static const std::string TAG_PK;

	static int pageSize;
	static int numberOfPages;
	static std::string pathForData;
	static TableMap tables;			// key: tableName value: map<columnName, dataType>
	static TableBlockMap tableBlocks;
	static std::map<std::string , std::vector<std::string> > tableMapList;
	
	static Table *getTableDataForTable(std::string tableName);
	static void readTableData(std::string tableName);
};

#endif /* CONFIGURATIONS_HPP_ */
