#include "configurations.hpp"

#include <algorithm/string/classification.hpp>
#include <algorithm/string/detail/classification.hpp>
#include <algorithm/string/split.hpp>
#include <algorithm/string/trim.hpp>
//#include <boost/algorithm/string.hpp>
#include <fstream>
#include <iostream>
#include <string>
#include <utility>

#include "dbconstants.hpp"
#include "Utils.hpp"

using namespace std;

int Configurations::pageSize;
int Configurations::numberOfPages;
string Configurations::pathForData;
TableMap Configurations::tables;
TableBlockMap Configurations::tableBlocks;
map<string, Table *> Configurations::tableDataMap;

string Configurations::CONFIG_FILE_PATH = "";

const string Configurations::TAG_PAGESIZE = "PAGE_SIZE";
const string Configurations::TAG_NUM_PAGES = "NUM_PAGES";
const string Configurations::TAG_PATH_FOR_DATA = "PATH_FOR_DATA";
const string Configurations::TAG_BEGIN_TABLE = "BEGIN";
const string Configurations::TAG_END_TABLE = "END";
const string Configurations::TAG_PK = "PRIMARY_KEY";

void Configurations::readTableData(string tableName) {
	if (!Utils::isTablePresent(tableName)) {
		cerr << tableName << " does not exist." << endl;
		return;
	}

	tableDataMap[tableName] = new Table(tableName);
}

Table *Configurations::getTableDataForTable(string tableName) {
	if(!Utils::isTablePresent(tableName)) {
		cerr << tableName << " does not exist." << endl;
		return NULL;
	}

	return tableDataMap[tableName];
}

Table::Table(string name) :
		tableName(name), totalRecords(0) {
	readColumns();
	readDistinctRecordValues();
}

void Table::processNewRecord(string recordStr) {
	vector<string> valuesVector;
	boost::split(valuesVector, recordStr, boost::is_any_of(","));

	for (unsigned i = 0; i < valuesVector.size(); i++) {
		string valueStr = valuesVector[i];
		boost::trim(valueStr);

		if (valueStr[0] == DOUBLE_QUOTE_CHAR
				&& valueStr[valueStr.size() - 1] == DOUBLE_QUOTE_CHAR)
			valueStr = valueStr.substr(1, valueStr.size() - 2);

		columnValuesSetVector[i]->insert(valueStr);
//		if(!columnValuesSetVector[i]->insert(valueStr).second)
//			cout << recordStr << " " << i << endl;
	}

	totalRecords++;
}

void Table::readDistinctRecordValues() {
	string filePath = Configurations::pathForData + "/" + tableName + ".csv";

	for (unsigned i = 0; i < columnNamesVector.size(); i++) {
		set<string> *valuesSet = new set<string>;
		columnValuesSetVector.push_back(valuesSet);
	}

	ifstream tableDataStream(filePath.c_str());

	string recordStr;
	while (!tableDataStream.eof()) {
		getline(tableDataStream, recordStr);
		boost::trim(recordStr);

		if (recordStr.empty())
			continue;

		processNewRecord(recordStr);
	}

	tableDataStream.close();
}

void Table::readColumns() {
	string filePath = Configurations::pathForData + "/" + tableName + ".data";

	ifstream tableSchemaIfs(filePath.c_str());
	string schemaStr;
	getline(tableSchemaIfs, schemaStr);
	tableSchemaIfs.close();

	vector<string> columnsWithTypeVector;
	boost::split(columnsWithTypeVector, schemaStr, boost::is_any_of(",:"));

	for (unsigned i = 0; i < columnsWithTypeVector.size(); i += 2) {
		string columnName = columnsWithTypeVector[i];
		boost::trim(columnName);
		columnNamesVector.push_back(columnName);
		columnIndexMap[columnName] = i / 2;
	}
}

string Table::getTableName() {
	return tableName;
}

long Table::getTotalRecords() {
	return totalRecords;
}

long Table::totalDistinctValues(string columnName) {
	map<string, unsigned>::iterator mapItr = columnIndexMap.find(columnName);
	if (mapItr == columnIndexMap.end())
		return -1;

	return columnValuesSetVector[columnIndexMap[columnName]]->size();
}
