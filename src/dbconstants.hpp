#ifndef _DBCONSTANTS_H
#define _DBCONSTANTS_H

const int PAGE_SIZE		= 4096;
const int MAX_NAME_SIZE	= 256;

#define DOUBLE_QUOTE_CHAR '\"'
#define SINGLE_QUOTE_CHAR '\''
#define UNDERSCORE_CHAR '_'
#define HYPHEN_CHAR '-'
#define DOT_CHAR '.'
#define SPACE_CHAR ' '
#define COMMA_CHAR ','
#define LBRACKET_CHAR '('
#define RBRACKET_CHAR ')'
#define COLON_CHAR ':'

#define OP_LESS_THAN '<'
#define OP_GREATER_THAN '>'
#define OP_EQUAL_TO '='
#define OP_LIKE "LIKE"

#define TYPE_INTEGER "INTEGER"
#define TYPE_VARCHAR "VARCHAR"
#define TYPE_TEXT "TEXT"
#define TYPE_FLOAT "FLOAT"


#endif
