#ifndef UTILS_HPP_
#define UTILS_HPP_

#include <vector>
#include <string>
#include <list>

class Utils {
public:
	static std::string type(std::string columnName,
			std::vector<std::string> listOfTables);
	static bool isColumnPresentInTable(std::string columnName,
			std::vector<std::string> listTables);
	static bool isTablePresent(std::string tableName);

	static std::string toString(int);
	static std::string trim(std::string &);
	
	static void create_dense_index( std::string table_name , std::string index_key );
	
	static int columnIndexInTable(std::string colName, std::string tableName);
	static std::string columnTypeInTable(std::string tableName, std::string columnName);
	static std::list<std::string> commonColumnsBetween(std::string table1, std::string table2);
};

#endif
